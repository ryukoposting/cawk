
BEGIN {
    FS="[ ]*=[ ]*"
    section = ""
}

function len(array) {
    _l = 0
    for (_i in array) _l++
    return _l
}

function error(message) {
    print "Error at line " NR ": " message | "cat 1>&2"
    exit -1;
}

function getkey(section, key) {
    return section "::" key
}

function writeto(text, file) {
    system("echo \"" text "\" > " file)
}

function appendto(text, file) {
    system("echo \"" text "\" >> " file)
}

function cmdcombine(commands, threads, simultcmd) {
    arrlen = len(commands)
    idx_ = 1
    while (arrlen > 0) {
        tcount = threads
        runcmd = ""
        
        for (n in commands) {
            runcmd = runcmd commands[n]
            tcount--
            arrlen--
            delete commands[n]
            if ((arrlen == 0) || (tcount == 0)) break
            runcmd = runcmd " & "
        }
        simultcmd[idx_] = runcmd
        idx_++
    }
#     return simultcmd
}

# section names
(/^\[.*\]/) {
    split($0, a, /\[/)
    split(a[2], a, /\]/)
    $0 = a[1]
    section = $0
}

# k/v pairs, v: str
(/^[^#]*=[ ]*".*"$/) {
    if (section == "") { error("no section specified for key/value pair") }
    if (section == "define") {
        cdefines[$1] = $2
    } else {
        split($0, a, "\"")
        gsub(/ /, "", $1)
        $2 = a[2]
        foo[getkey(section, $1)] = $2
    }
}

# k/v pairs, v: [str]
(/^[^#]*=[ ]*\[.*\]$/) {
    if (section == "") { error("no section specified for key/value pair") }
    if (section == "define") {
        cdefines[$1] = $2
    } else {
        gsub(/ /, "", $1)
        gsub(/^[ ]*\[/, "", $2)
        gsub(/\][ ]*$/, "", $2)
        gsub(/"[ ]*"/, "\n", $2)
        gsub(/"/, "", $2)       #"
        foo[getkey(section, $1)] = $2
    }
}

# k/v pairs, v: num
(/^[^#]*=[ ]*[\.01234356789bxo]*$/) {
    if (section == "") { error("no section specified for key/value pair") }
    if (section == "define") {
        cdefines[$1] = $2
    } else {
        gsub(/ /, "", $1)
        gsub(/ /, "", $2)
        foo[getkey(section, $1)] = $2
    }
}

END {
    if (!("project::output" in foo))    { error("project output directory unspecified") }
    if (!("project::name" in foo))      { error("project name unspecified") }
    if (!("project::root" in foo))      { error("project root unspecified") }
    
    if (!("toolchain::prefix" in foo))  { foo["toolchain::prefix"] = "" }
    if (!("toolchain::cc" in foo))      { foo["toolchain::cc"] = foo["toolchain::prefix"] "gcc" }
    if (!("toolchain::cxxc" in foo))    { foo["toolchain::cxxc"] = foo["toolchain::prefix"] "g++" }
    if (!("toolchain::as" in foo))      { foo["toolchain::as"] = foo["toolchain::prefix"] "as" }
    if (!("toolchain::ld" in foo))      { foo["toolchain::ld"] = foo["toolchain::prefix"] "ld" }
    if (!("toolchain::objcopy" in foo)) { foo["toolchain::objcopy"] = foo["toolchain::prefix"] "objcopy" }
    if (!("toolchain::objdump" in foo)) { foo["toolchain::objdump"] = foo["toolchain::prefix"] "objdump" }
    if (!("toolchain::nm" in foo))      { foo["toolchain::nm"] = foo["toolchain::prefix"] "nm" }
    
    if (!("lang::c" in foo))            { foo["lang::c"] = ".*\\.c$" }
    if (!("lang::cxx" in foo))          { foo["lang::cxx"] = ".*\\.(cc|cxx|cpp)$" }
    if (!("lang::asm" in foo))          { foo["lang::asm"] = ".*\\.(s|S)$" }
    
    if (!("misc::threads" in foo))      { "grep -c ^processor /proc/cpuinfo" | getline foo["misc::threads"] }
    
    if (!("flags::c" in foo))           { foo["flags::c"] = "-Wall -Wextra -Wredundant-decls -Warray-bounds=2 -Wundef -Wnested-externs -Werror=return-type -Werror=logical-op -Werror=array-bounds -Werror=reorder -Werror=type-limits -Werror=cast-qual -Werror=cast-align -Werror=logical-not-parentheses -Werror=override-init -Wno-restrict -fextended-identifiers -fno-exceptions -std=c11" }
    if (!("flags::cxx" in foo))           { foo["flags::cxx"] = "-Wall -Wextra -Wredundant-decls -Woverloaded-virtual -Wundef -Warray-bounds=2 -Werror=return-type -Werror=logical-op -Werror=array-bounds -Werror=reorder -Werror=type-limits -Werror=cast-qual -Werror=cast-align -Werror=logical-not-parentheses -Werror=override-init -Wno-restrict -fextended-identifiers -fno-exceptions -std=c++14" }
    
    # add linker script to compiler flags
    if ("misc::ldscript" in foo) { 
        foo["flags::c"] = foo["flags::c"] " -T" foo["misc::ldscript"] 
        foo["flags::cxx"] = foo["flags::cxx"] " -T" foo["misc::ldscript"]
    }
    
    # add includes to compiler flags
    if ("project::includes" in foo) {
        split(foo["project::includes"], includes, "\n")
        for (x in includes) {
            foo["flags::c"] = foo["flags::c"] " -I" includes[x]
            foo["flags::cxx"] = foo["flags::cxx"] " -I" includes[x]
        }
    }
    
    # generate the project file manifest generator, src.sh
    system("mkdir " foo["project::output"] " 2> /dev/null")
    
    split(foo["project::sources"], sources, "\n")
    srcsh = "find " foo["project::root"] " -print "
    for (x in sources) {
        srcsh = srcsh " | grep -E \\\"" sources[x] "\\\""
    }
    srcsh = srcsh " > " foo["project::output"] "/manifest.txt"
    writeto(srcsh, foo["project::output"] "/src.sh")
    
    search[1] = "\ncat " foo["project::output"] "/manifest.txt | grep -E \\\"" foo["lang::c"] "\\\" > " foo["project::output"] "/manifest_c.txt"
    search[2] = "\ncat " foo["project::output"] "/manifest.txt | grep -E \\\"" foo["lang::cxx"] "\\\" > " foo["project::output"] "/manifest_cxx.txt"
    search[3] = "\ncat " foo["project::output"] "/manifest.txt | grep -E \\\"" foo["lang::asm"] "\\\" > " foo["project::output"] "/manifest_asm.txt"
    cmdcombine(search, foo["misc::threads"], simultcmd)
    
    for (k in simultcmd) { appendto(simultcmd[k] "\n", foo["project::output"] "/src.sh") }
    
    
    # generate the defines file
    writeto("//WARNING: This file is automatically generated by cawk.", foo["project::root"] "/cawk.h")
    appendto("// Changes to this file will be overwritten next time cawk.sh is called.", foo["project::root"] "/cawk.h")
    appendto("#ifndef _CAWKDEFS_H_\n#define _CAWKDEFS_H_\n", foo["project::root"] "/cawk.h")
    for (x in cdefines) {
        appendto("#define " x " " cdefines[x] "\n", foo["project::root"] "/cawk.h")
    }
    appendto("#endif", foo["project::root"] "/cawk.h")
    
    
    # dump cawkdata.txt
    cawkdatatxt = foo["project::root"] "/" foo["project::output"] "/cawkdata.txt"
    writeto("", cawkdatatxt)
    for (x in foo) {
        if (foo[x] !~ /\n/) { 
            appendto(x "=" foo[x],  cawkdatatxt)
        }
    }
    
}
